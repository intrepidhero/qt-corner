"""
    Cells demonstrates a basic spreadsheet. In this case the spreadsheet has two "layers",
    formulas and values. Formulas are valid python expressions, values are results of 
    evaluating those expressions. Cells may be referred to by their column and row, for
    example, A0 is the left most, top cell.

"""
import io
import re
import tokenize
from collections import defaultdict

from PySide2.QtWidgets import (
    QApplication,
    QTableView,
    QStackedLayout,
    QMenuBar,
    QMenu,
    QAction,
    QWidget,
    QMainWindow,
)
from PySide2.QtGui import QKeySequence
from PySide2.QtCore import Qt, QAbstractTableModel


ALEPH = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
COLUMNS = list(ALEPH)
for a in ALEPH:
    for b in ALEPH:
        COLUMNS.append(a + b)
COLUMNS = tuple(COLUMNS)


class PyTableResolver:
    """ Takes two models with dictionary type interfaces, one for source and for results.
    resolves any python in the source model (including cell references of the form
    XXYY: where XX is the column designation using above COLUMNS and YY is an
    an integer row index starting from 0.
    """

    def __init__(self):
        pass

    def resolve(self, source, result, context):
        queue = []
        numdeps = defaultdict(lambda: 0)
        depgraph = defaultdict(list)
        for cell in source:
            # find any dependcies in this cell
            if len(source[cell]) == 0:
                continue
            for token in tokenize.generate_tokens(io.StringIO(source[cell]).readline):
                if token.type == tokenize.NAME and not token.string in context:
                    dep = self.convert_cell_ref_to_index(token.string)
                    numdeps[cell] += 1
                    depgraph[dep].append(cell)
            if numdeps[cell] == 0:
                queue.append(cell)
        visited_count = 0
        ordered_cells = []
        while queue:
            cell = queue.pop(0)
            ordered_cells.append(cell)
            for req in depgraph[cell]:
                numdeps[req] -= 1
                if numdeps[req] == 0:
                    queue.append(req)
            visited_count += 1
        if visited_count > len(source):
            raise Exception("Cyclic dependency graph")

        # build localscope and assemble python code
        localscope = {}
        globalscope = {}
        sourcelines = []
        for cell in ordered_cells:
            sourcelines.append(
                self.convert_cell_index_to_ref(cell) + " = " + source[cell]
            )
        sourcecode = "\n".join(sourcelines)
        print(sourcecode)
        # execute it
        code = compile(sourcecode, "filename", "exec")
        exec(code, globalscope, localscope)
        # save results to result
        for cell in localscope:
            result[self.convert_cell_ref_to_index(cell)] = localscope[cell]

    def convert_cell_ref_to_index(self, ref):
        # converts an XXYY ref to (column, row) index
        match = re.fullmatch("([A-Z]+)([0-9]+)", ref)
        if not match:
            print("Failed to parse: ", ref)
        col = COLUMNS.index(match.group(1))
        row = int(match.group(2))
        return (col, row)

    def convert_cell_index_to_ref(self, index):
        # converts (column, row) index to XXYY ref
        return COLUMNS[index[0]] + str(index[1])


class PyTableModel(QAbstractTableModel):
    """ Override QAbstractTableModel and extend with a dictionary like interface """

    def __init__(self):
        QAbstractTableModel.__init__(self)
        self.pytable = {}  # keys are tuples (column, row)
        for row in range(100):
            for column in range(len(ALEPH)):
                self.pytable[(column, row)] = ""

    def data(self, index, role):
        if role == Qt.DisplayRole:
            return self.pytable[(index.column(), index.row())]

    def flags(self, index):
        return Qt.ItemIsEditable | Qt.ItemIsSelectable | Qt.ItemIsEnabled

    def setData(self, index, value, role):
        if role == Qt.EditRole:
            self.pytable[(index.column(), index.row())] = value
            self.dataChanged.emit(index, index)  # why twice?
            return True
        return False

    def headerData(self, section, orientation, role):
        if role == Qt.DisplayRole:
            if orientation == Qt.Vertical:
                return str(section)
            return COLUMNS[section]
        return None

    def rowCount(self, parent):
        # plus one because my row/column keys are 0 based
        # use max because the table is a sparse matrix
        return max([i[1] for i in self.pytable.keys()]) + 1

    def columnCount(self, parent):
        # plus one because my row/column keys are 0 based
        # use max because the table is a sparse matrix
        return max([i[0] for i in self.pytable.keys()]) + 1

    def __getitem__(self, key):
        return self.pytable[key]

    def __setitem__(self, key, value):
        self.pytable[key] = value

    def __len__(self):
        return len(self.pytable)

    def __iter__(self):
        return self.pytable.__iter__()


class PyTableWidget(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        self.dataModel = PyTableModel()
        self.formulaModel = PyTableModel()
        self.dataView = QTableView()
        self.dataView.setModel(self.dataModel)
        self.formulaView = QTableView()
        self.formulaView.setModel(self.formulaModel)
        self.lo = QStackedLayout(self)
        self.lo.addWidget(self.dataView)
        self.lo.addWidget(self.formulaView)
        self.lo.setCurrentWidget(self.dataView)

    def populateMenu(self, menuBar):
        menuBar["view"].addAction("Show &values", self.showValues)
        menuBar["view"].addAction("Show &formulas", self.showFormulas)
        menuBar["run"].addAction("Recalculate", self.recalc)

    def recalc(self):
        r = PyTableResolver()
        r.resolve(self.formulaModel, self.dataModel, {})

    def showValues(self):
        self.lo.setCurrentWidget(self.dataView)

    def showFormulas(self):
        self.lo.setCurrentWidget(self.formulaView)


class HashMapMenuBar(QMenuBar):
    def __init__(self):
        QMenuBar.__init__(self)
        self.menus = {}

    def addMenu(self, key, menuTitle):
        menu = QMenu(menuTitle)
        self.menus[key] = menu
        QMenuBar.addMenu(self, menu)

    def __getitem__(self, key):
        return self.menus[key]


class Gui(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        self.pytable = PyTableWidget()
        self.setCentralWidget(self.pytable)
        self.setupMenu()

    def setupMenu(self):
        menuBar = HashMapMenuBar()
        menuBar.addMenu("view", "&View")
        menuBar.addMenu("run", "&Run")
        self.setMenuBar(menuBar)
        self.pytable.populateMenu(menuBar)


class Main:
    def __init__(self):
        app = QApplication([])
        gui = Gui()
        gui.show()
        app.exec_()


if __name__ == "__main__":
    Main()
