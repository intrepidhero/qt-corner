# Qt Corner

A place to store my experiments with Qt.

Each folder here represents a separate experiment. Most contain a single file python script showcasing either a particular facet of Qt/PySide or a particular UI design that I wanted to play with.

To run these scripts you will need python version 3 and PySide2. I am currently using 3.7.3 and 5.13.1 respectively. In general Things should work fairly well with any Python 3+ and PySide2 (or PyQt5 for that matter) but your mileage my vary.

This project structure is not recommended for production level code and test coverage is virtually non-existant. However the point of the qt-corner repository is to have a place to share my experiments in Qt and UI design and it is something I work on in my (very limited) free time. As such it is optimized for being able to quickly write proof of concepts and showcase ideas mostly in isolation.

For further information and discussion see the source code and comments in the repository and articles posted on [my website](http://fadedbluesky.com/tech/).
