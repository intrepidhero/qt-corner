import re
import io
import os
import sys
import shlex
import yaml
import argparse
import subprocess
from collections import defaultdict

from PySide2.QtWidgets import (
    QApplication,
    QWidget,
    QMainWindow,
    QTextEdit,
    QLineEdit,
    QPushButton,
    QStatusBar,
    QMenu,
    QMenuBar,
    QAction,
    QToolTip,
    QVBoxLayout,
    QHBoxLayout,
    QLabel,
    QFileDialog,
    QDockWidget,
)

from PySide2.QtGui import (
    QSyntaxHighlighter,
    QTextDocument,
    QTextCharFormat,
    QBrush,
    QColor,
    QTextBlockUserData,
    QTextCursor,
    QKeySequence,
    QFont,
)
from PySide2.QtCore import Qt, QRegularExpression

from pygments.token import Token, string_to_tokentype
from pygments.lexers import PythonLexer

EXEDIR = os.path.split(__file__)[0]
try:
    STYLESHEET = open(os.path.join(EXEDIR, "style.css")).read()
except:
    STYLESHEET = """
QWidget {
    background-color: #111111;
    color: #EEEEEE;
    font-family: "DejaVu Sans Mono";
    font-size: 13pt;
    border: 1px solid black;
    padding: 0;
    margin: 0;
}
QTextEdit {
    background-color: #000000;
}
QMenuBar {
    font-weight: bold;
}
"""

# color, italic, bold
HILITESTYLES = {
    "Comment": (0x00AA00, True, False),
    "Literal.String.Doc": (0x00AA00, True, False),
    "Keyword": (0xAA00FF, False, False),
    "Name": (0xFFFFFF, False, False),
    "Name.Function": (0xAA5500, False, False),
    "Name.Class": (0xAAAA00, False, True),
    "Literal.String": (0x5555FF, False, False),
    "Literal": (0xAA0000, False, False),
    "Operator": (0xAA0000, False, False),
    "Text": (0xFFFFFF, False, False),
}
try:
    HILITESTYLES.update(yaml.safe_load(open(os.path.join(EXEDIR, "hilite.yaml"))))
except Exception as e:
    print(str(e))


class SearchBox(QLineEdit):
    def __init__(self, statusBar, editTool):
        QLineEdit.__init__(self)
        self.statusBar = statusBar
        self.editTool = editTool

    def focusOutEvent(self, event):
        self.statusBar.removeWidget(self)

    def keyPressEvent(self, event):
        key = event.key()
        if key == Qt.Key_Escape:
            self.focusOutEvent(None)
        elif key == Qt.Key_Return:
            needle = self.text()
            self.editTool.search(needle, repeat=True)
        QLineEdit.keyPressEvent(self, event)
        needle = self.text()
        self.editTool.search(needle)


def makeFormat(foreground, italic=False, bold=False, underline=False, background=None):
    """ create a QTextCharFormat based on some common attributes """
    tcFormat = QTextCharFormat()
    tcFormat.setForeground(QBrush(QColor(foreground)))
    if background:
        tcFormat.setBackground(QBrush(QColor(background)))
    if italic:
        tcFormat.setFontItalic(True)
    if bold:
        tcFormat.setFontWeight(100)
    return tcFormat


class UserData(QTextBlockUserData):
    """ Stores the highlighter state of the current textblock """

    def __init__(self, stack):
        QTextBlockUserData.__init__(self)
        self.stack = stack


class Highlighter(QSyntaxHighlighter):
    """ Pygments based highlighter class """

    def __init__(self, doc):
        QSyntaxHighlighter.__init__(self, doc)
        self.lex = PythonLexer()
        # WARNING: a typo on Token.XX not throw an error
        self.defaultStyle = makeFormat(*HILITESTYLES["Text"])
        self.styles = {}
        for tokenString, props in HILITESTYLES.items():
            self.styles[string_to_tokentype(tokenString)] = makeFormat(*props)

    def styleMatcher(self, token):
        match = None
        for tokenType in self.styles:
            if token in tokenType and (not match or len(tokenType) > len(match)):
                match = tokenType
        return match

    def highlightBlock(self, text):
        block = self.currentBlock()
        userdata = block.previous().userData()
        if not userdata or not userdata.stack:
            stack = ("root",)
        else:
            stack = userdata.stack
        for r in self.lex.get_tokens_unprocessed(text, stack):
            i, tt, v, stack = r
            match = self.styleMatcher(tt)
            if match:
                self.setFormat(i, len(v), self.styles[match])
            else:
                # This is here to fix the weird missing lines bug
                # that I noticed on some nested tuples but had
                # trouble finding a minimal example that reproduced
                # it's not related to block.setVisible
                # and setting the textCharFormat fixed it
                # it's unclear to me what happening
                self.setFormat(i, len(v), self.defaultStyle)
        self.setCurrentBlockUserData(UserData(stack))


class Editor(QTextEdit):
    def __init__(self, ctrl):
        QTextEdit.__init__(self)
        self.ctrl = ctrl
        self.toolKey = "edit"
        self.mode = "insert"
        self.filename = None

        self.autoIndentRE = ".+:$"
        self.tab = "    "
        self.newline = "\n"

    def setFilename(self, filename):
        self.filename = filename
        self.ctrl.gui.updateTitleBar(filename)
        self.document().setModified(False)

    def load(self, filename):
        self.setPlainText(open(filename).read())
        if filename.endswith(".py"):
            self.hl = Highlighter(self.document())
        self.setFilename(filename)

    def save(self):
        if self.filename:
            open(self.filename, "w").write(self.toPlainText())
            result = subprocess.run(
                shlex.split("python3 -m black " + self.filename), capture_output=True
            )
            self.ctrl.gui.displayUserMessage(
                result.stderr.decode("utf-8").splitlines()[0]
            )
            pos = self.textCursor().position()
            scrollPos = self.verticalScrollBar().value()
            self.load(self.filename)
            tc = self.textCursor()
            tc.setPosition(pos)
            self.setTextCursor(tc)
            self.verticalScrollBar().setValue(scrollPos)

    def saveAs(self, filename):
        self.setFilename(filename)
        self.save()

    def new(self):
        self.filename = None
        self.setPlainText("")

    def search(self, exp, direction=None, repeat=False):
        if not repeat:
            tc = self.textCursor()
            tc.setPosition(tc.anchor())
            # if you don't do this it searches from the end of the current selection
            # messing up search as you type
            self.setTextCursor(tc)
        regExp = QRegularExpression(exp)
        flags = None
        secondPos = self.document().begin().position()
        if direction == "back":
            flags |= QTextDocument.FindBackward
            secondPos = self.document().end().position()
        if flags:
            found = self.find(QRegularExpression(exp), flags)
        else:
            found = self.find(QRegularExpression(exp))
        if not found:
            # try again with wrapping
            if flags:
                tc = self.document().find(regExp, secondPos, flags)
            else:
                tc = self.document().find(regExp, secondPos)
            if tc.hasSelection():
                self.setTextCursor(tc)

    def keyPressEvent(self, event):
        key = event.key()
        if key == Qt.Key_Tab:
            self.insertPlainText(self.tab)
        elif key == Qt.Key_Backtab:
            tc = self.textCursor()
            for f in range(len(self.tab)):
                tc.movePosition(QTextCursor.PreviousCharacter, QTextCursor.KeepAnchor)
                possibleTab = tc.selectedText()
                if possibleTab == self.tab:
                    tc.deleteChar()
        elif key == Qt.Key_Return:
            tc = self.textCursor()
            tc.movePosition(QTextCursor.StartOfLine, QTextCursor.KeepAnchor)
            line = tc.selectedText()
            indent = 0
            while line.startswith(self.tab):
                indent += 1
                line = line[len(self.tab) :]
            if re.match(self.autoIndentRE, line):
                indent += 1
            tc.movePosition(QTextCursor.EndOfLine)
            tc.insertText(self.newline + self.tab * indent)
        else:
            QTextEdit.keyPressEvent(self, event)
        if self.document().isModified():
            self.ctrl.gui.updateTitleBar("{}*".format(self.filename))


class ActionBar(QMenuBar):
    def __init__(self, actions):
        QMenuBar.__init__(self)
        self.menus = {}
        for menuText, items in actions:
            menuObj = QMenu(menuText)
            for text, handler, shortcut in items:
                menuObj.addAction(text, handler, shortcut)
            self.addMenu(menuObj)
            key = menuText.lower().replace("&", "")
            self.menus[menuText] = menuObj


class PythonHelper(QDockWidget):
    AUTOLIBS = (
        "",
        "PySide2.QtWidgets.",
        "PySide2.QtGui.",
        "PySide2.QtCore.",
        "os.",
        "sys.",
    )

    def __init__(self, title, parent, ctrl):
        QDockWidget.__init__(self, title, parent)
        self.setAllowedAreas(Qt.LeftDockWidgetArea | Qt.RightDockWidgetArea)
        self.ctrl = ctrl
        main = QWidget(self)
        self.content = QTextEdit()
        self.content.setReadOnly(True)
        self.content.setFocusPolicy(Qt.NoFocus)
        self.cli = QLineEdit()
        lo = QVBoxLayout(main)
        lo.addWidget(self.content)
        lo.addWidget(self.cli)
        self.setWidget(main)

        self.cli.returnPressed.connect(self.resolve)

    def show(self):
        QDockWidget.show(self)
        self.cli.setFocus()

    def resolve(self):
        # Have to be sneaky here because help() writes to stdout
        for prefix in PythonHelper.AUTOLIBS:
            sneaky_io = io.StringIO()
            sys.stdout = sneaky_io
            val = prefix + self.cli.text()
            try:
                help(val)
            except:
                sys.stdout = sys.__stdout__
                raise
            finally:
                sys.stdout = sys.__stdout__
            result = sneaky_io.getvalue()
            if not result.startswith("No Python"):
                self.content.setText(result)
                break


class MainGui(QMainWindow):
    def __init__(self, ctrl):
        QMainWindow.__init__(self)
        self.ctrl = ctrl
        # self.setStyleSheet(STYLESHEET)

        self.editor = Editor(ctrl)
        self.setCentralWidget(self.editor)
        self.pyhelper = PythonHelper("PyHelp", self, ctrl)
        self.pyhelper.hide()
        self.actions = (
            (
                "&File",
                (
                    ("&New", self.editor.new, QKeySequence("Ctrl+N")),
                    ("&Open", self.open, QKeySequence("Ctrl+O")),
                    ("&Save", self.editor.save, QKeySequence("Ctrl+S")),
                    ("Save &As", self.saveAs, QKeySequence("Shift+Ctrl+S")),
                    ("&Quit", self.quit, QKeySequence("Ctrl+Q")),
                ),
            ),
            (
                "&Edit",
                (
                    ("&Copy", self.editor.copy, QKeySequence("Ctrl+C")),
                    ("Cu&t", self.editor.cut, QKeySequence("Ctrl+X")),
                    ("&Paste", self.editor.paste, QKeySequence("Ctrl+V")),
                    ("&Find", self.search, QKeySequence("Ctrl+F")),
                    ("&Replace", self.replace, QKeySequence("Ctrl+H")),
                ),
            ),
            ("&Help", (("&PyHelp", self.togglePyHelp, QKeySequence("F1")),),),
        )
        self.actionBar = ActionBar(self.actions)
        self.setMenuBar(self.actionBar)
        self.displayUserMessage("Ready")
        self.updateTitleBar("<No file>")
        self.searchBox = None
        self.setStyleSheet(STYLESHEET)

    def open(self):
        result = QFileDialog.getOpenFileName(
            self, options=QFileDialog.DontUseNativeDialog
        )
        if not result:
            return
        self.editor.load(result[0])

    def saveAs(self):
        result = QFileDialog.getSaveFileName(
            self, options=QFileDialog.DontUseNativeDialog
        )
        if not result:
            return
        self.editor.saveAs(result[0])

    def quit(self):
        self.close()

    def search(self):
        statusBar = self.statusBar()
        if self.searchBox:
            statusBar.removeWidget(self.searchBox)
        statusBar.clearMessage()
        searchBox = SearchBox(statusBar, self.editor)
        statusBar.addWidget(searchBox)
        self.searchBox = searchBox
        searchBox.setFocus()

    def replace(self):
        pass

    def displayUserMessage(self, message):
        self.statusBar().showMessage(message, 5000)

    def updateTitleBar(self, message):
        self.setWindowTitle(message)

    def togglePyHelp(self):
        if self.dockWidgetArea(self.pyhelper) == Qt.NoDockWidgetArea:
            self.pyhelper.show()
            self.addDockWidget(Qt.RightDockWidgetArea, self.pyhelper)
        else:
            self.removeDockWidget(self.pyhelper)


class Main:
    def __init__(self):
        self.gui = None

        app = QApplication([])
        font = app.font()
        font.setFamily("DejaVu Sans Mono Book")
        font.setFixedPitch(True)
        font.setPointSize(13)
        font.setWeight(QFont.Light)
        font.setStyleStrategy(QFont.PreferAntialias)
        app.setFont(font)
        self.gui = MainGui(self)
        self.gui.showMaximized()
        app.exec_()


if __name__ == "__main__":
    Main()
